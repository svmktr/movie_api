const express = require('express')
const app = express()
const movieRoutes = require('./api/routes/movie')
const directorRoutes = require('./api/routes/director')


app.use('/api/movies', movieRoutes)
app.use('/api/directors', directorRoutes)


// app.use('/movies', movieRoutes)


module.exports = app;
