'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('movies', {
      Rank: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      Title: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      Runtime: {
        type: Sequelize.INTEGER
      },
      Genre: {
        type: Sequelize.STRING
      },
      Rating: {
        type: Sequelize.FLOAT
      },
      Metascore: {
        type: Sequelize.INTEGER
      },
      Votes: {
        type: Sequelize.INTEGER
      },
      Gross_Earning_in_Mil: {
        type: Sequelize.FLOAT
      },
      Director: {
        type: Sequelize.STRING
      },
      Actor: {
        type: Sequelize.STRING
      },
      Year: {
        type: Sequelize.INTEGER
      }
    },{timestamps:false});
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('movies');
  }
};