'use strict';
const {movie} = require('../movie')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('movies',movie)
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('movies', null, {});
  
  }
};
