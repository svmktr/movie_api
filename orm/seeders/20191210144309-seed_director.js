'use strict';
const {director} = require('../movie');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('directors',director)
   
  },

  down: (queryInterface, Sequelize) => {    
      return queryInterface.bulkDelete('directors', null, {});

  }
}
