const winston = require('winston');

// Logger configuration
const logConfiguration = {
  transports: [
    new winston.transports.File({
      filename: 'd:/MountBlue/project/orm/api/logger/winstonlogs.log',
    }),
  ],
};

// Create the logger
const logger = winston.createLogger(logConfiguration);

module.exports = { logger };