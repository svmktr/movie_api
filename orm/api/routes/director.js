const express = require('express');
const router = express.Router();
const db = require('../../models');
const fs = require('fs');
const path = require('path');
router.use(express.json());
const { logger } = require('../logger/winston');
const morgan = require('morgan');
const director = db.directors;


//  Morgan logging
router.use(
  morgan('common', {
    stream: fs.createWriteStream(path.join("d:/MountBlue/project/orm/api/logger/", 'access.log'), {
      flags: 'a'
    })
  })
);




// Get all Directors:
router.get('/', function (req, res) {
  director.findAll().then(function (results) {
    res.send(results)
  }).catch(error => {
    logger.error({ message: `${error} enter correct id` });
    res.sendStatus(500);
  });
});



// Get Director by ID:
router.get('/:id', function (req, res) {
  director.findOne({ where: { id: req.params.id } })
    .then(results => {
      if (results !== null) {
        res.send(results);
      } else if (results === null) {
        res.status(404).send("Id does not Exist");
        logger.error({ message: "Id does not Exist" });
      }
    })
    .catch(error => {
      logger.error({ message: `${error} enter correct id` });
      res.sendStatus(500);
    });
});


// POST : to add a new director

router.post('/', function (req, res) {
  director.create({
    director: req.body.director
  })
    .then(function (results) {
      res.status(201).send(`Added on id ${results.id}`);
    }).catch(error => {
      logger.error({ message: `${error} in adding director` });
      res.sendStatus(500);
    });
});


// DELETE : to remove a director

router.delete('/:id', function (req, res) {
  director
    .destroy({ where: { id: req.params.id } })
    .then(results => {
      if (results === 1) {
        res.status(410).send('Deleted Succesfully');
      } else if (results === 0) {
        res.status(404).send('Id does not Exist');
        logger.error({ message: 'id not found' });
      }
    })
    .catch(error => {
      logger.error({ message: `${error} in deleting this id` });
      res.sendStatus(500);
    });
});


// PUT : to update details of a director

router.put('/:id', function (req, res) {
  director.update(
    { director: req.body.director },
    { where: { id: req.params.id } }
  )
    .then(function (results) {
      if (results[0] != 0) {
        res.sendStatus(202);
      }
      else {
        res.status(403).send('Id does not Exist');
        logger.error({ message: 'choose different id' });
      }
    })
    .catch(error => {
      logger.error({ message: `${error} in updating director by id` });
      res.sendStatus(500);
    });
});


module.exports = router;