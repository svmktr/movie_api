const express = require('express');
const router = express.Router();
const db = require('../../models');
const fs = require('fs');
const path = require('path');
router.use(express.json());
const { logger } = require('../logger/winston');
const morgan = require('morgan');
const movie = db.movies;


//  Morgan logging:
router.use(
  morgan('common', {
    stream: fs.createWriteStream(path.join("d:/MountBlue/project/orm/api/logger/", 'access.log'), {
      flags: 'a'
    })
  })
);



// Get all movies:

router.get('/', function (req, res) {
  movie.findAll()
    .then(results => {
      res.send(results);
    })
    .catch(err => {
      logger.log(err.stack);
      res.sendStatus(500);
    });
});


// Get Movie by ID:

router.get('/:id', function (req, res) {
  movie.findOne({ where: { Rank: req.params.id } })
    .then(results => {
      if (results !== null) {
        res.send(results);
      } else if (results === null) {
        res.sendStatus(404);
        logger.error({ message: 'id not found' });
      }
    })
    .catch(error => {
      logger.error({ message: `${error} enter correct id` });
      res.sendStatus(500);
    });
});


// Post a new Movie:
router.post('/', function (req, res) {
  movie.create({
    Title: req.body.Title,
    Description: req.body.Description,
    Runtime: req.body.Runtime,
    Genre: req.body.Genre,
    Rating: req.body.Rating,
    Metascore: req.body.Metascore,
    Votes: req.body.Votes,
    Gross_Earning_in_Mil: req.body.Gross_Earning_in_Mil,
    Director: req.body.Director,
    Actor: req.body.Actor,
    Year: req.body.Year
  })
    .then(function (results) {
      res.status(201).send(`Added Successfully`);
    })
    .catch(error => {
      logger.error({ message: `${error} in adding movie` });
      res.sendStatus(500);
    });
});



// Delete a Movie:
router.delete('/:id', function (req, res) {
  movie.destroy({ where: { Rank: req.params.id } })
    .then(function (results) {
      if (results === 1) {
        console.log(results)
        res.status(410).send('Deleted Successfully');
      } else if (results === 0) {
        res.status(404).send('Id does not Exist');
        logger.error({ message: 'id not found' });
      }
    })
});


// Update a Movie:

router.put('/:id', function (req, res) {
  movie.update({
    Title: req.body.Title,
    Description: req.body.Description,
    Runtime: req.body.Runtime,
    Genre: req.body.Genre,
    Rating: req.body.Rating,
    Metascore: req.body.Metascore,
    Votes: req.body.Votes,
    Gross_Earning_in_Mil: req.body.Gross_Earning_in_Mil,
    Director: req.body.Director,
    Actor: req.body.Actor,
    Year: req.body.Year
  },
    { where: { Rank: req.params.id } })
    .then(results => {
      if (results[0] != 0) {
        res.status(202).send('Updated Successfully');

      } else {
        res.status(403).send('Id does not Exist');
      }
    })
    .catch(error => {
      logger.error({ message: `${error} in updating movie by id` });
      res.sendStatus(500);
    });
});

// Exporting this file to the Router:
module.exports = router;