var mysql = require('mysql');
// const fs = require('fs')
let json_data = require('./movies.json')

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "veronica@",
    database: 'movies'
});

let count = 1
con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    for (let i of json_data) {
        let values = []
        for (let j in i) {
            if (i[j] === 'NA') {
                i[j] = null
            }
            values.push(i[j])
        }
        let sql = "INSERT INTO movies_data (Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Director, Actor, Year) VALUES ?";
        con.query(sql, [[values]], function (err, result) {
        if (err) throw err;
        console.log("Number of records inserted: " + count);
        count += 1
      });   
}
con.end()
});