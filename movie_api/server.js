const http = require('http')
const app = require('./app.js')
const port = process.env.port || 9000;
const server = http.createServer(app)


server.listen(port, function(error){
    if (error){
        console.log('something is wrong', error)
    }
    else{
        console.log('server is listening on port: ', port)
    }
})