const express = require('express')
const app = express()
const movieRoutes = require('./api/routes/movies')
const directorRoutes = require('./api/routes/directors')


app.use('/api/movies', movieRoutes)
app.use('/api/directors', directorRoutes)


// app.use('/movies', movieRoutes)


module.exports = app;