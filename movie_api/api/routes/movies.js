const express = require('express')
const router = express.Router();
const mysql = require('mysql');
router.use(express.json());

// Conneting to Database:
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'veronica@',
  database: 'movies'
});

con.connect(err => {
  if (err) throw err;
  console.log('Connected Movies');
});


// Get all Movies:
router.get('/', (req, res, next) => {
  let sql = 'SELECT * FROM movies_data';
  con.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
})


// Get single Movie by ID:
router.get('/:id', (req, res, next) => {
  let sql = `SELECT * FROM movies_data where Rank = ${req.params.id}`;
  con.query(sql, function (err, result) {
    if (err) throw err;
    if (result.length != 0) {
      res.status(200).send(result);
    }
    else {
      res.status(404).send("Id Does not exist");
    }
  });
});


// Add a new Movie:
router.post('/', (req, res) => {
  const values = [];
  values.push([
    req.body.Rank,
    req.body.Title,
    req.body.Description,
    req.body.Runtime,
    req.body.Genre,
    req.body.Rating,
    req.body.Metascore,
    req.body.Votes,
    req.body.Gross_Earning_in_Mil,
    req.body.Director,
    req.body.Actor,
    req.body.Year
  ]);
  let sql = `INSERT into movies_data (Rank, Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Director, Actor, Year) VALUES ?`;

  con.query(sql, [values], (err, result) => {
    if (err) throw err;
    res.status(201).send("New Movie added Succesfully");
  });
});


// Update Movie:
router.put('/:id', (req, res) => {
  const { body } = req;
  const data = {
    rank: body.Rank,
    title: body.Title,
    desc: body.Description,
    time: body.Runtime,
    genre: body.Genre,
    rating: body.Rating,
    metascore: body.Metascore,
    votes: body.Votes,
    gross: body.Gross_Earning_in_Mil,
    director: body.Director,
    actor: body.Actor,
    year: body.Year
  };
  let sql = `UPDATE movies_data SET Rank= ${data.rank}, Title = '${data.title}', Description = '${data.desc}', Runtime= ${data.time}, Genre= '${data.genre}', Rating= ${data.rating}, Metascore= ${data.metascore}, Votes= ${data.votes}, Gross_Earning_in_Mil= ${data.gross}, Director= '${data.director}', Actor= '${data.actor}', Year= ${data.year} where Rank = ${req.params.id}`;
  con.query(sql, (err, result) => {
    if (err) throw err;
    if (result['affectedRows'] != 0) {
      res.status(205).send("Updated Succesfully");
    }
    else {
      res.status(404).send("Id Does Not Exist")
    }
  });
});



// Delete Movie:
router.delete('/:id', (req, res) => {
  let sql = `DELETE FROM movies_data where rank = ${req.params.id}`;
  con.query(sql, (err, result) => {
    if (err) throw err;
    if (result['affectedRows'] != 0) {
      res.status(410).send("Deleted Succesfully");
    }
    else {
      res.status(404).send("Id does not Exist");
    }
  });
});


// Exporting this file to the Router:
module.exports = router; 