const express = require('express')
const router = express.Router();
const mysql = require('mysql');
router.use(express.json());



// Connecting to Databas:
const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'veronica@',
    database: 'movies'
});
con.connect(err => {
    if (err) throw err;
    console.log('Connected Directors');
});


// Get all Directors:
router.get('/', (req, res, next) => {

    let sql = 'SELECT * FROM director';
    con.query(sql, function (err, result) {
        if (err) throw err;
        res.status(200).send(result);
    });
})


// Get single Director by ID:
router.get('/:id', (req, res, next) => {
    let sql = `SELECT * FROM director where Rank = ${req.params.id}`;
    con.query(sql, function (err, result) {
        if (err) throw err;
        if (result.length != 0) {
            // console.log(result)
            res.status(200).send(result);
        }
        else {
            res.status(404).send("Id Does not Exist");
        }
    });
});


// Add a new Director:
router.post('/', (req, res) => {
    let sql = `Insert into director (director) Values ('${req.body.Director}')`;
    con.query(sql, function (err, result) {
        if (err) throw err;
        if (result['affectedRows'] != 0) {
            res.status(201).send("New Director added Succesfully");
        }
        else {
            res.status(422).send("Invalid Request");
        }
    });
});


// Update a Director:
router.put('/:id', (req, res) => {
    let sql = `UPDATE director set director = '${req.body.Director}' where Rank = ${req.params.id}`;
    con.query(sql, function (err, result) {
        if (err) throw err;
        if (result['affectedRows'] != 0) {
            res.status(205).send("Updated Succesfully");
        }
        else {
            res.status(404).send("Id does not Exist")
        }
    });
});


// Delete a Director:
router.delete('/:id', (req, res) => {
    let sql = `DELETE FROM director where rank = ${req.params.id}`;
    con.query(sql, (err, result) => {
        if (err) throw err;
        if (result['affectedRows'] != 0) {
            res.status(410).send("Deleted Succesfully");
        }
        else {
            res.status(404).send("Id does not Exist");
        }
    });
});


// This file Exporting to the Router:
module.exports = router; 